﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int cote1 = 0;
            int cote2 = 0;
            int cote3 = 0;

            Console.WriteLine("Vous devez entrer trois entiers positifs représentant les côtés d'un triangle.");
            Console.WriteLine("Veuillez entrer le premier côté: ");

            try //on s'assure que les entrées sont des entiers
            {
                cote1 = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Vous n'avez pas entré un nombre valide.");
                Console.ReadLine();
                Environment.Exit(-1);
            }

            Console.WriteLine("Veuillez entrer le deuxième côté: ");

            try
            {
                cote2 = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Vous n'avez pas entré un nombre valide.");
                Console.ReadLine();
                Environment.Exit(-1);
            }

            Console.WriteLine("Veuillez entrer le troisième côté: ");

            try
            {
                cote3 = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Vous n'avez pas entré un nombre valide.");
                Console.ReadLine();
                Environment.Exit(-1);
            }

            //On vérifie que les entiers sont positifs
            if (cote1 < 0 || cote2 < 0 || cote3 < 0)
            {
                Console.WriteLine("Vous n'avez pas entré 3 entiers positifs.");
            }
            else
            {
                string resultat = ValidationTriangle(cote1, cote2, cote3);
                if (resultat == "PasUnTriangle")
                    Console.WriteLine("Les côtés ne forment pas un triangle.");
                else
                    Console.WriteLine("Les côtés forment un triangle " + resultat + ".");
            }

            Console.ReadLine();
        }

        static string ValidationTriangle(int cote1, int cote2, int cote3)
        {
            string typeTriangle = "";

            //la longueur d'un côté est toujours inférieure à la somme des longueurs des deux autres côtés.
            if ((cote1 + cote2 <= cote3) || (cote2 + cote3 <= cote1) ||
              (cote1 + cote3 <= cote2))
            {
                typeTriangle = "PasUnTriangle";
                return typeTriangle;
            }
            else
            {   //Tous les côtés sont égaux
                if ((cote1 == cote2) && (cote2 == cote3))
                {
                    typeTriangle ="équilatéral";
                    return typeTriangle;
                }
                else
                {   //Tous les côtés sont différents
                    if ((cote1 != cote2) && (cote2 != cote3) && (cote1 != cote3))
                    {
                        typeTriangle = "quelconque";
                        return typeTriangle;
                    }
                    else
                    {
                        typeTriangle = "isocèle";
                        return typeTriangle;
                    }
                }
            }

        }
    }
}
